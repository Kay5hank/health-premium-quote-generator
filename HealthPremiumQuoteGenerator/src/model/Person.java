package model;

import java.util.List;

public class Person {

	private String name;
	private Gender gender;
	private int age;
	private List<HealthIssue> healthIssueList;
	private List<Habit> habitList;
	
	
	public Person(String name, Gender gender, int age, List<HealthIssue> healthIssueList, List<Habit> habitList) {
		super();
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.healthIssueList = healthIssueList;
		this.habitList = habitList;
	}
	
	public List<HealthIssue> getHealthIssueList() {
		return healthIssueList;
	}
	public void setHealthIssueList(List<HealthIssue> healthIssueList) {
		this.healthIssueList = healthIssueList;
	}
	public List<Habit> getHabitList() {
		return habitList;
	}
	public void setHabitList(List<Habit> habitList) {
		this.habitList = habitList;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	
}
