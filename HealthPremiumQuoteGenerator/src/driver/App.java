package driver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import model.Gender;
import model.Habit;
import model.HealthIssue;
import model.Person;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Person person=new 
				Person("Norman Gomes",Gender.MALE,34,
						new ArrayList<HealthIssue>(Arrays.asList(HealthIssue.OVERWEIGHT)),
						new ArrayList<Habit>(Arrays.asList(Habit.ALCOHOL,Habit.DAILYEXERCISE)) );

		String prefix=null;
		if(person.getGender() == Gender.MALE ) {
			prefix="Mr.";
		}
		else if(person.getGender() == Gender.FEMALE ) {
			prefix="Mrs.";
		}
		else {
			prefix="Mx.";
		}
		String lastName=person.getName().split(" ")[1];
		System.out.println("Health Insurance Premium for "+prefix+" "+lastName+" : Rs. "+calculatePremium(person));
	}

	public static int calculatePremium(Person person) {
	    //base premium
	    int premium = 5000;

	    //premium calculation based upon age
	    if (person.getAge() >= 18) {
	        premium *= 1.1;//+10%
	        //premium=premium+0.1*premium=1.1*premium
	        
	       
	    }
	    //18-25
	    if (person.getAge() >= 25) {
	        premium *= 1.1;//+10%
	    }
	    //25-30
	    if (person.getAge() >= 30) {
	        premium *= 1.1;//+10%
	    }
	    //30-35
	    if (person.getAge() >= 35) {
	        premium *= 1.1;//+10% 	
	    }
	    //40+
	    if (person.getAge() >= 40) {
	       
	        int diff = person.getAge() - 40;
	        while (diff >= 5) {
	            premium *= 1.2;//20% increase every 5 year above 40
	            diff -= 5;
	        }
	    }

	    //premium calculation based upon gender
	    if (person.getGender() == Gender.MALE) {
	        premium *= 1.02;//2% increase for males
	    }

	    //precondition check
	    for (HealthIssue issue : person.getHealthIssueList()) {
	        premium *= 1.01;
	    }
	    //habits
	    List<Habit> habitList = person.getHabitList();
	   
	    for (Habit habit : habitList) {
	        if (habit == Habit.DAILYEXERCISE) {
	            premium *= 0.97;//good habit->3% reduction
	        }
	    }

	    for (Habit habit : habitList) {
	        if (habit == Habit.ALCOHOL | habit == Habit.DRUGS | habit == Habit.SMOKING) {
	            premium *= 1.03;//bad habit->3% increment
	        }
	    }

	    return premium;
	}
}
